#https://github.com/CentOS/CentOS-Dockerfiles
#https://github.com/CentOS/CentOS-Dockerfiles/tree/master/httpd/centos7
FROM centos:latest as production
ARG PDCI_BASE_COMMIT_SHORT_SHA
#ARG PDCI_COMMIT_MESSAGE
ARG PDCI_BASE_COMMIT_TAG
#MAINTAINER ICMBIO <roquebrasilia@gmail.com>
LABEL Vendor=""
LABEL PDCI_BASE_COMMIT_TAG=${PDCI_BASE_COMMIT_SHORT_SHA}
LABEL PDCI_BASE_COMMIT_SHORT_SHA=${PDCI_BASE_COMMIT_TAG}
#LABEL License=GPLv2
#LABEL Version=0.0.1
#LABEL Features="1 melhoria x meria y"
#LABEL tag=""

#RUN yum --setopt=tsflags=nodocs update -y
#RUN yum  clean all
RUN yum --setopt=tsflags=nodocs install epel-release -y && \
    yum --setopt=tsflags=nodocs install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y  && \
    yum --setopt=tsflags=nodocs install httpd -y && \
    yum clean all && \
    rm -rf /var/cache/yum

#Instalacao do POSTFIX
RUN yum --setopt=tsflags=nodocs install postfix -y && \
    yum clean all && \
    rm -rf /var/cache/yum

RUN sed -i "s|#relayhost = uucphost|relayhost = mail.icmbio.gov.br|g" /etc/postfix/main.cf && \
    sed -i "s|inet_interfaces = localhost|#inet_interfaces = localhost|g" /etc/postfix/main.cf && \
    sed -i "s|#inet_interfaces = all|inet_interfaces = all|g" /etc/postfix/main.cf && \
    mkfifo /var/spool/postfix/public/pickup

RUN chkconfig postfix on
RUN postfix start

EXPOSE 443 80

ADD run-httpd.sh /run-httpd.sh
RUN chmod -v +x /run-httpd.sh

CMD ["/run-httpd.sh"]

FROM production as testing
#MAINTAINER ICMBIO <roquebrasilia@gmail.com>

# Instalacao do node para os testes
RUN curl --silent --location https://rpm.nodesource.com/setup_8.x | bash -  && \
     yum install -y --setopt=tsflags=nodocs lynx net-tools git maven ant wget automake autoconf  dh-autoreconf nasm libjpeg-turbo-utils pngquant nodejs && \
     yum clean all && \
     rm -rf /var/cache/yum && \
     npm  install -g  yarn



CMD ["/run-httpd.sh"]